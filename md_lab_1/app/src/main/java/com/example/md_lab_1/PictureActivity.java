package com.example.md_lab_1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.icu.text.SimpleDateFormat;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.File;
import java.io.IOException;
import java.util.Date;

public class PictureActivity extends AppCompatActivity {
    private static final int CAMERA_REQUEST = 1888;
    int markerId;
    ImageView imageView;
    TextView noPhotoText;
    Uri imageUri;

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,
                ".jpg",
                storageDir
        );
        return image;
    }

    private void takePicture() throws IOException {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        imageUri = FileProvider.getUriForFile(
                this,
                getApplicationContext().getPackageName() + ".provider",
                createImageFile()
        );
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        startActivityForResult(intent, CAMERA_REQUEST);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.picture_activity);

        Intent intent = getIntent();
        Uri markerPhoto = intent.getParcelableExtra("MARKER_PHOTO");
        markerId = intent.getIntExtra("MARKER_INDEX", -1);

        LinearLayout verticalLayout = findViewById(R.id.container);
        Button updatePhotoButton = findViewById(R.id.buttonUpdate);
        noPhotoText = findViewById(R.id.textViewNoPhoto);

        imageView = new ImageView(this);
        imageView.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT,
                1f
        ));
        if (markerPhoto != null) {
            imageUri = markerPhoto;
            try {
                imageView.setImageBitmap(MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri));
            }
            catch (java.io.IOException exception) {
                //
            }
            noPhotoText.setVisibility(View.INVISIBLE);
        }

        verticalLayout.addView(imageView);

        updatePhotoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    takePicture();
                }
                catch (java.io.IOException exception) {
                    finish();
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK)
        {
            Bitmap photo = null;
            try {
                photo = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
                imageView.setImageBitmap(photo);
                noPhotoText.setVisibility(View.INVISIBLE);
            }
            catch (java.io.IOException exception) {
                finish();
            }

            Intent result_data = new Intent();
            result_data.putExtra("NEW_IMAGE", imageUri);
            result_data.putExtra("MARKER_INDEX", markerId);
            setResult(RESULT_OK, result_data);
        }
    }
}
