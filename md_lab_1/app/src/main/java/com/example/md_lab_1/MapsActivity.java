package com.example.md_lab_1;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.Objects;
import java.util.UUID;

import io.realm.DynamicRealmObject;
import io.realm.Realm;
import io.realm.RealmResults;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {
    private static final int PICTURE_ACTIVITY_REQUEST = 1222;
    private GoogleMap mMap;
    private ArrayList<MarkerParcelable> mapMarkers;
    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (mapMarkers == null) {
            mapMarkers = new ArrayList<>();
        }
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        Realm.init(this);
        realm = Realm.getDefaultInstance();
    }

    private void openPictureActivity(Marker marker) {
        Intent intent = new Intent(this, PictureActivity.class);
        int markerIndex = (int) marker.getTag();
        Uri markerPhoto = null;
        if (mapMarkers.get(markerIndex).getPhoto() != null)
            markerPhoto = Uri.parse(mapMarkers.get(markerIndex).getPhoto());
        intent.putExtra("MARKER_PHOTO", markerPhoto);
        intent.putExtra("MARKER_INDEX", markerIndex);
        startActivityForResult(intent, PICTURE_ACTIVITY_REQUEST);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (mapMarkers.size() == 0) {
            RealmResults<MarkerParcelable> markerParcelables = realm.where(MarkerParcelable.class).findAll();
            mapMarkers = new ArrayList<>(markerParcelables);
        }

        if (mapMarkers.size() != 0)
            for (int i = 0; i < mapMarkers.size(); i++) {
                MarkerParcelable markerData = mapMarkers.get(i);
                Marker marker = mMap.addMarker(new MarkerOptions().position(markerData.getPosition()));
                marker.setTag(i);
            }

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(final LatLng latLng) {
                Marker marker = mMap.addMarker(new MarkerOptions().position(latLng));
                final String markerId = UUID.randomUUID().toString();
                mapMarkers.add(new MarkerParcelable(null, latLng, markerId));
                marker.setTag(mapMarkers.size() - 1);

                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        MarkerParcelable marker = realm.createObject(MarkerParcelable.class, markerId);
                        marker.setPosition(latLng);
                        marker.setPhoto(null);
                    }
                });
            }
        });

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                mMap.moveCamera(CameraUpdateFactory.newLatLng(marker.getPosition()));
                openPictureActivity(marker);
                return true;
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == PICTURE_ACTIVITY_REQUEST && resultCode == Activity.RESULT_OK)
        {
            final int markerIndex = (int) Objects.requireNonNull(data.getExtras()).get("MARKER_INDEX");
            final Uri photo = (Uri) Objects.requireNonNull(data.getExtras()).get("NEW_IMAGE");

            MarkerParcelable markerParcelable = mapMarkers.get(markerIndex);
            final String markerId = markerParcelable.getId();

            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    MarkerParcelable affectedMarker = realm.where(MarkerParcelable.class).equalTo("id", markerId).findFirst();
                    if (affectedMarker != null && photo != null)
                        affectedMarker.setPhoto(photo.toString());
                    mapMarkers.set(markerIndex, affectedMarker);
                }
            });
        }
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
//        mapMarkers.clear();
//        mapMarkers = savedInstanceState.getParcelableArrayList("MARKER_DATA");
//        realm = Realm.getDefaultInstance();
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        //outState.putParcelableArrayList("MARKER_DATA", mapMarkers);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        realm.close();
        super.onDestroy();
    }
}
