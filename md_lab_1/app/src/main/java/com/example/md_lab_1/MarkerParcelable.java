package com.example.md_lab_1;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class MarkerParcelable extends RealmObject implements Parcelable {
    @PrimaryKey
    private String id;

    private String photo;
    private Double lat;
    private Double lng;

    public String getPhoto() {
        return photo;
    }

    public LatLng getPosition() {
        return new LatLng(lat, lng);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public void setPosition(LatLng position) {
        this.lat = position.latitude;
        this.lng = position.longitude;
    }

    public MarkerParcelable(Uri photo, LatLng position, String id) {
        this.lat = position.latitude;
        this.lng = position.longitude;
        this.id = id;

        if (photo != null)
            this.photo = photo.toString();
        else
            this.photo = null;
    }

    public MarkerParcelable() {
    }


    protected MarkerParcelable(Parcel in) {
        id = in.readString();
        photo = in.readString();
        lat = in.readByte() == 0x00 ? null : in.readDouble();
        lng = in.readByte() == 0x00 ? null : in.readDouble();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(photo);
        if (lat == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeDouble(lat);
        }
        if (lng == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeDouble(lng);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<MarkerParcelable> CREATOR = new Parcelable.Creator<MarkerParcelable>() {
        @Override
        public MarkerParcelable createFromParcel(Parcel in) {
            return new MarkerParcelable(in);
        }

        @Override
        public MarkerParcelable[] newArray(int size) {
            return new MarkerParcelable[size];
        }
    };
}
