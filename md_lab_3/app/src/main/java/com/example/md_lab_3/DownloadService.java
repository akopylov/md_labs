package com.example.md_lab_3;

import android.app.DownloadManager;
import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;

import androidx.annotation.Nullable;

import java.net.URL;
import java.net.URLConnection;
import java.util.Objects;

public class DownloadService extends Service {
    String desiredUrl;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        desiredUrl = intent.getStringExtra("URL");
        downloadFile();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void downloadFile() {
        Util.CreateNotificationChannel(this);

        try {
            // Create URL and connect
            Uri downloadUri = Uri.parse(desiredUrl);
            DownloadManager.Request request = new DownloadManager.Request(downloadUri);
            request.setTitle(downloadUri.getLastPathSegment());

            request.allowScanningByMediaScanner();
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_HIDDEN);

            request.setDestinationInExternalFilesDir(getApplicationContext(), "/folder", downloadUri.getLastPathSegment());

            DownloadManager manager = (DownloadManager) getSystemService(this.DOWNLOAD_SERVICE);
            Objects.requireNonNull(manager).enqueue(request);
            if (DownloadManager.STATUS_SUCCESSFUL == 8) {
                Util.CreateNotificationChannel(this);
                Util.Notify(this, "Notify", "Download completed");
            }
        }
        catch (Exception e) {
            Util.Notify(this, "Alert", "Something went wrong during download");
        }
        finally {
            stopSelf();
        }
    }
}
