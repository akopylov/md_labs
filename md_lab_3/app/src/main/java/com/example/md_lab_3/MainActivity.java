package com.example.md_lab_3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.Button;

import com.google.android.material.textfield.TextInputEditText;

import java.net.URL;

public class MainActivity extends AppCompatActivity {
    Button downloadButton;
    TextInputEditText textInputUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        downloadButton = findViewById(R.id.buttonDownload);
        textInputUrl = findViewById(R.id.textInputUrl);

        //Apply listeners
        downloadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDownloadButtonClick();
            }
        });
    }

    public void onDownloadButtonClick() {
        Intent intent = new Intent(this, DownloadService.class);
        String url = textInputUrl.getText().toString();
        if (!URLUtil.isValidUrl(url)) {
            Util.CreateNotificationChannel(this);
            Util.Notify(this, "Alert", "Provided url is not valid!");
            return;
        }

        intent.putExtra("URL", url);
        startService(intent);
    }
}
